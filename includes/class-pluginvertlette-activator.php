<?php

/**
 * Fired during plugin activation
 *
 * @link       vertlette.dev.local
 * @since      1.0.0
 *
 * @package    Pluginvertlette
 * @subpackage Pluginvertlette/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pluginvertlette
 * @subpackage Pluginvertlette/includes
 * @author     GT Thibault <germbault@gmail.com>
 */
class Pluginvertlette_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
