<?php

/**
 * Fired during plugin deactivation
 *
 * @link       vertlette.dev.local
 * @since      1.0.0
 *
 * @package    Pluginvertlette
 * @subpackage Pluginvertlette/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pluginvertlette
 * @subpackage Pluginvertlette/includes
 * @author     GT Thibault <germbault@gmail.com>
 */
class Pluginvertlette_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
