<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       vertlette.dev.local
 * @since      1.0.0
 *
 * @package    Pluginvertlette
 * @subpackage Pluginvertlette/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Pluginvertlette
 * @subpackage Pluginvertlette/includes
 * @author     GT Thibault <germbault@gmail.com>
 */
class Pluginvertlette_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'pluginvertlette',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
